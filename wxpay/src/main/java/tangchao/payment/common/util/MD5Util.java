package tangchao.payment.common.util;

import java.security.MessageDigest;

/**
 * @author johnny
 * @ClassName: MD5Util
 * @Description: MD5加密工具 用于微信支付
 * @date 2017年3月14日 下午3:56:29
 */
public class MD5Util {
	
	private final static String[] hexDigits = {"0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
	
	/**
	 * 转换字节数组为16进制字串
	 *
	 * @param b 字节数组
	 * @return 16进制字串
	 */
	public static final String byteArrayToHexString(byte[] b) {
		final StringBuilder resultSb = new StringBuilder();
		for (byte aB : b) {
			resultSb.append(byteToHexString(aB));
		}
		return resultSb.toString();
	}
	
	/**
	 * 转换byte到16进制
	 *
	 * @param b 要转换的byte
	 * @return 16进制格式
	 */
	private static final String byteToHexString(byte b) {
		int n = b;
		if (n < 0) {
			n = 256 + n;
		}
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}
	
	/**
	 * MD5编码
	 *
	 * @param origin 原始字符串
	 * @return 经过MD5加密之后的结果
	 */
	public static final String MD5Encode(String origin) {
		String resultString = null;
		try {
			resultString = origin;
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byteArrayToHexString(md.digest(resultString.getBytes("utf-8")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultString;
	}
	
	/**
	 * MD5编码
	 *
	 * @param origin 原始二进制字符串
	 * @return 经过MD5加密之后的结果
	 */
	public static final String MD5Encode(byte[] origin) {
		String resultString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byteArrayToHexString(md.digest(origin));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultString;
	}
}
