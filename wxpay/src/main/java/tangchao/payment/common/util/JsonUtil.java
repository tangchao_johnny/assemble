package tangchao.payment.common.util;

import java.util.Map;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 11:57
 */
public class JsonUtil {
	
	public static final String mapToJson(final Map<String, Object> map) {
		final StringBuffer sb = new StringBuffer();
		sb.append("{");
		if (map != null && map.size() > 0) {
			for (final Map.Entry<String, Object> m : map.entrySet()) {
				sb.append("\"").append(m.getKey()).append("\"");
				sb.append(":");
				sb.append("\"").append(m.getValue()).append("\"");
				sb.append(",");
			}
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("}");
		return sb.toString();
	}
}
