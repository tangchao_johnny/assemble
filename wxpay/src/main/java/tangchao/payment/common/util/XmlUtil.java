package tangchao.payment.common.util;

import java.util.Map;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 11:52
 */
public class XmlUtil {
	
	public static final String mapToXml(final Map<String, Object> map) {
		final StringBuffer sb = new StringBuffer();
		sb.append("<xml>");
		if (map != null && map.size() > 0) {
			for (Map.Entry<String, Object> m : map.entrySet()) {
				sb.append("<").append(m.getKey()).append(">");
				sb.append("<![CDATA[").append(m.getValue()).append("]]>");
				sb.append("</").append(m.getKey()).append(">");
			}
		}
		sb.append("</xml>");
		return sb.toString();
	}
}
