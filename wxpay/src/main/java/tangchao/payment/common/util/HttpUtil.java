package tangchao.payment.common.util;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import tangchao.payment.exception.PaymentException;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by 唐潮(johnny)
 * 2018/11/28 10:24
 */
public class HttpUtil {
	
	public static final String HOST = "api.mch.weixin.qq.com";
	
	public static final String httpPost(final String url, final String xml) throws PaymentException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			httpclient = HttpClients.createDefault();
			
			HttpPost httpost = new HttpPost(url); // 设置响应头信息
			httpost.addHeader("Connection", "keep-alive");
			httpost.addHeader("Accept", "*/*");
			httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpost.addHeader("Host", HOST);
			httpost.addHeader("X-Requested-With", "XMLHttpRequest");
			httpost.addHeader("Cache-Control", "max-age=0");
			httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
			httpost.setEntity(new StringEntity(xml, "UTF-8"));
			
			response = httpclient.execute(httpost);
			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(response.getEntity(), "UTF-8");
			EntityUtils.consume(entity);
			return result;
		} catch (FileNotFoundException fnfe) {
			throw new PaymentException(fnfe);
		} catch (IOException ioe) {
			throw new PaymentException(ioe);
		} finally {
			try {
				if (httpclient != null) {
					httpclient.close();
				}
				if (response != null) {
					response.close();
				}
			} catch (IOException ioe) {
				throw new PaymentException(ioe);
			}
		}
	}
	
	public static final String httpPostCertificate(final String url, final String xml, final String mchId, final String certificatePath) throws PaymentException {
		
		FileInputStream instream = null;
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			/**
			 * 注意PKCS12证书 是从微信商户平台-》账户设置-》 API安全 中下载的
			 */
			KeyStore keyStore = KeyStore.getInstance("PKCS12");
			instream = new FileInputStream(new File(certificatePath));// P12文件目录
			
			keyStore.load(instream, mchId.toCharArray());// 这里写密码..默认是你的MCHID
			
			// Trust own CA and all self-signed certs
			SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(new TrustStrategy() {//忽略掉对服务器端证书的校验
				public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
					return true;
				}
				//加载服务端提供的truststore(如果服务器提供truststore的话就不用忽略对服务器端证书的校验了)
			}).loadKeyMaterial(keyStore, mchId.toCharArray()).build();// 这里也是写密码的
			
			// Allow TLSv1 protocol only
			SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslcontext, new String[]{"TLSv1"}, null,
					SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			
			httpclient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
			
			HttpPost httpost = new HttpPost(url); // 设置响应头信息
			httpost.addHeader("Connection", "keep-alive");
			httpost.addHeader("Accept", "*/*");
			httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpost.addHeader("Host", HOST);
			httpost.addHeader("X-Requested-With", "XMLHttpRequest");
			httpost.addHeader("Cache-Control", "max-age=0");
			httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
			httpost.setEntity(new StringEntity(xml, "UTF-8"));
			
			response = httpclient.execute(httpost);
			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(response.getEntity(), "UTF-8");
			EntityUtils.consume(entity);
			return result;
		} catch (KeyStoreException kse) {
			throw new PaymentException(kse);
		} catch (FileNotFoundException fnfe) {
			throw new PaymentException(fnfe);
		} catch (CertificateException ce) {
			throw new PaymentException(ce);
		} catch (IOException ioe) {
			throw new PaymentException(ioe);
		} catch (NoSuchAlgorithmException nsae) {
			throw new PaymentException(nsae);
		} catch (UnrecoverableKeyException uke) {
			throw new PaymentException(uke);
		} catch (KeyManagementException kme) {
			throw new PaymentException(kme);
		} finally {
			try {
				if (instream != null) {
					instream.close();
				}
				if (httpclient != null) {
					httpclient.close();
				}
				if (response != null) {
					response.close();
				}
			} catch (IOException ioe) {
				throw new PaymentException(ioe);
			}
		}
	}
}
