package tangchao.payment.check.util;

import tangchao.payment.annotation.*;
import tangchao.payment.exception.PaymentException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 15:17
 */
public class AnnotaionUtils {
	
	public static final <T> List<String> check(T object) throws PaymentException {
		try {
			final List<String> errorList = new ArrayList<>();
			
			final List<Field> allFieldsList = getAllFieldList(object.getClass());
			for (Field field : allFieldsList) {
				field.setAccessible(true);//对私有字段的访问取消权限检查。暴力访问。
				errorList.addAll(parsingAnnotation(field.get(object), field.getAnnotations()));
			}
			return errorList;
		} catch (IllegalAccessException iae) {
			throw new PaymentException(iae);
		}
	}
	
	private static final List<Field> getAllFieldList(final Class<?> cls) {
		final List<Field> fieldList = new ArrayList<Field>();
		Class<?> currentClass = cls;
		while (currentClass != null) {
			final Field[] declaredFields = currentClass.getDeclaredFields();
			for (final Field field : declaredFields) {
				fieldList.add(field);
			}
			currentClass = currentClass.getSuperclass();
		}
		return fieldList;
	}
	
	private static final List<String> parsingAnnotation(Object obj, Annotation[] annotations) {
		final List<String> errorList = new ArrayList<>();
		for (Annotation annotation : annotations) {
			if (annotation instanceof NotBlank) {
				NotBlank notBlank = (NotBlank) annotation;
				if (obj == null || (obj + "").trim() == "") {
					errorList.add(notBlank.message());
				}
			} else if (annotation instanceof Size) {
				Size size = (Size) annotation;
				if (obj != null && (obj.toString()).trim() != "") {
					if ((size.min() > 0 && obj.toString().length() < size.min())
							|| (size.max() < 2147483647 && obj.toString().length() > size.max())) {
						errorList.add(size.message());
					}
				}
			} else if (annotation instanceof Min) {
				Min min = (Min) annotation;
				if (obj != null && Long.parseLong(obj + "") < min.value()) {
					errorList.add(min.message());
				}
			} else if (annotation instanceof Max) {
				Max max = (Max) annotation;
				if (obj != null && Long.parseLong(obj + "") > max.value()) {
					errorList.add(max.message());
				}
			} else if (annotation instanceof Pattern) {
				Pattern pattern = (Pattern) annotation;
				if (obj != null && !java.util.regex.Pattern.compile(pattern.regexp()).matcher(obj + "").matches()) {
					errorList.add(pattern.message());
				}
			}
			
		}
		return errorList;
	}
	
}
