package tangchao.payment.exception;

/**
 * Created by 唐潮(johnny)
 * 2018/11/27 15:42
 */
public class PaymentException extends Exception {
	//无参构造方法
	public PaymentException() {
		super();
	}
	
	//有参的构造方法
	public PaymentException(String message) {
		super(message);
	}
	
	// 用指定的详细信息和原因构造一个新的异常
	public PaymentException(String message, Throwable cause) {
		super(message, cause);
	}
	
	//用指定原因构造一个新的异常
	public PaymentException(Throwable cause) {
		super(cause);
	}
}
