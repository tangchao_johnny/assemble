package tangchao.payment.wxpay;

import tangchao.payment.exception.PaymentException;
import tangchao.payment.wxpay.entity.coupon.SendCoupon;
import tangchao.payment.wxpay.entity.coupon.QueryCouponInfo;
import tangchao.payment.wxpay.entity.coupon.QueryCouponStock;
import tangchao.payment.wxpay.enumerate.InterfaceType;
import tangchao.payment.wxpay.enumerate.ResultType;

/**
 * Created by 唐潮(johnny)
 * 2018/12/29 10:00
 */
public class CouponApi extends Pay {
	
	/**
	 * 发放代金券
	 * @param sendCoupon        发放代金券参数
	 * @param certificatePath   证书全路径(微信支付的.p12证书)
	 * @param certificateKey    证书密钥，如果为空则取mch_id
	 * @param resultType        结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object sendcoupon(SendCoupon sendCoupon, String certificatePath, String certificateKey, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/send_coupon";
		sendCoupon.setNonceStr(getNonceStr());
		checkObj(sendCoupon);
		// 对参数进行签名
		sendCoupon.setSign();
		return requestCertificate(url, sendCoupon.toXml(), certificatePath, certificateKey, InterfaceType.SENDCOUPON, resultType);
	}
	
	/**
	 * 查询代金券批次
	 * @param queryCouponStock    查询代金券批次参数
	 * @param resultType          结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object querycouponstock(QueryCouponStock queryCouponStock, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/query_coupon_stock";
		queryCouponStock.setNonceStr(getNonceStr());
		checkObj(queryCouponStock);
		// 对参数进行签名
		queryCouponStock.setSign();
		return request(url, queryCouponStock.toXml(), InterfaceType.QUERYCOUPONSTOCK, resultType);
	}
	
	/**
	 * 查询代金券信息
	 * @param queryCouponInfo   查询代金券信息参数
	 * @param resultType        结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object querycouponsinfo(QueryCouponInfo queryCouponInfo, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/querycouponsinfo";
		queryCouponInfo.setNonceStr(getNonceStr());
		checkObj(queryCouponInfo);
		// 对参数进行签名
		queryCouponInfo.setSign();
		return request(url, queryCouponInfo.toXml(), InterfaceType.QUERYCOUPONINFO, resultType);
	}

}
