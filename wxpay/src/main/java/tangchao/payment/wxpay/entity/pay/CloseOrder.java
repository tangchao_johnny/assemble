package tangchao.payment.wxpay.entity.pay;

import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Size;
import tangchao.payment.wxpay.entity.Base;


/**
 * Created by 唐潮(johnny)
 * 2018/11/29 10:21
 */
public class CloseOrder extends Base {
	
	/**
	 * 公众账号ID
	 */
	@NotBlank(message = "[appid]公众账号ID不能为空")
	@Size(max = 32, message = "[appid]公众账号ID最大长度为32个字符")
	private String appId;
	
	/**
	 * 商户号
	 */
	@NotBlank(message = "[mch_id]商户号不能为空")
	@Size(max = 32, message = "[mch_id]商户号最大长度为32个字符")
	private String mchId;
	
	/**
	 * 商户订单号
	 */
	@NotBlank(message = "[out_trade_no]商户订单号不能为空")
	@Size(max = 32, message = "[out_trade_no]商户订单号最大长度为32个字符")
	private String outTradeNo;
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		this.valueMap.put("appid", appId);
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
		this.valueMap.put("mch_id", mchId);
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
		this.valueMap.put("out_trade_no", outTradeNo);
	}
}
