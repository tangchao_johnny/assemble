package tangchao.payment.wxpay.entity.pay;

import tangchao.payment.annotation.Min;
import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Size;
import tangchao.payment.wxpay.entity.Base;

/**
 * Created by 唐潮(johnny)
 * 2018/11/28 16:19
 */
public class Refund extends Base {
	
	/**
	 * 公众账号ID
	 */
	@NotBlank(message= "[appid]公众账号ID不能为空")
	@Size(max = 32, message= "[appid]公众账号ID最大长度为32个字符")
	private String appId;
	
	/**
	 * 商户号
	 */
	@NotBlank(message= "[mch_id]商户号不能为空")
	@Size(max = 32, message= "[mch_id]商户号最大长度为32个字符")
	private String mchId;
	
	/**
	 * 微信订单号
	 */
	@Size(max = 32, message= "[transaction_id]微信订单号最大长度为32个字符")
	private String transactionId;
	
	/**
	 * 商户订单号
	 */
	@Size(max = 32, message= "[out_trade_no]商户订单号最大长度为32个字符")
	private String outTradeNo;
	
	/**
	 * 商户退款单号
	 */
	@NotBlank(message= "[out_refund_no]商户退款单号不能为空")
	@Size(max = 64, message= "[out_refund_no]商户退款单号最大长度为64个字符")
	private String outRefundNo;
	
	/**
	 * 订单金额
	 */
	@NotBlank(message= "[total_fee]订单金额不能为空")
	@Min(value = 1, message= "[total_fee]订单金额最小单位为1分")
	private Integer totalFee;
	
	/**
	 * 退款金额
	 */
	@NotBlank(message= "[refund_fee]退款金额不能为空")
	@Min(value = 1, message= "[refund_fee]退款金额最小单位为1分")
	private Integer refundFee;
	
	/**
	 * 退款货币种类
	 */
	@Size(max = 8, message= "[refund_fee_type]退款货币种类最大长度为8个字符")
	private String refundFeeType;
	
	/**
	 * 退款原因
	 */
	@Size(max = 80, message= "[refund_desc]退款原因最大长度为80个字符")
	private String refundDesc;
	
	/**
	 * 退款资金来源
	 */
	@Size(max = 30, message= "[refund_account]退款资金来源最大长度为30个字符")
	private String refundAccount;
	
	/**
	 * 退款结果通知url
	 */
	@Size(max = 256, message= "[notify_url]退款结果通知url最大长度为256个字符")
	private String notifyUrl;
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		this.valueMap.put("appid", appId);
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
		this.valueMap.put("mch_id", mchId);
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
		this.valueMap.put("transaction_id", transactionId);
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
		this.valueMap.put("out_trade_no", outTradeNo);
	}
	
	public String getOutRefundNo() {
		return outRefundNo;
	}
	
	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
		this.valueMap.put("out_refund_no", outRefundNo);
	}
	
	public Integer getTotalFee() {
		return totalFee;
	}
	
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
		this.valueMap.put("total_fee", totalFee);
	}
	
	public Integer getRefundFee() {
		return refundFee;
	}
	
	public void setRefundFee(Integer refundFee) {
		this.refundFee = refundFee;
		this.valueMap.put("refund_fee", refundFee);
	}
	
	public String getRefundFeeType() {
		return refundFeeType;
	}
	
	public void setRefundFeeType(String refundFeeType) {
		this.refundFeeType = refundFeeType;
		this.valueMap.put("refund_fee_type", refundFeeType);
	}
	
	public String getRefundDesc() {
		return refundDesc;
	}
	
	public void setRefundDesc(String refundDesc) {
		this.refundDesc = refundDesc;
		this.valueMap.put("refund_desc", refundDesc);
	}
	
	public String getRefundAccount() {
		return refundAccount;
	}
	
	public void setRefundAccount(String refundAccount) {
		this.refundAccount = refundAccount;
		this.valueMap.put("refund_account", refundAccount);
	}
	
	public String getNotifyUrl() {
		return notifyUrl;
	}
	
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
		this.valueMap.put("notify_url", notifyUrl);
	}
}
