package tangchao.payment.wxpay.entity.pay;

import tangchao.payment.exception.PaymentException;
import tangchao.payment.wxpay.entity.Base;
import tangchao.payment.wxpay.enumerate.InterfaceType;

import java.util.Map;

/**
 * Created by 唐潮(johnny)
 * 2018/11/28 15:23
 */
public class Result extends Base {
	
	/**
	 * 检测签名
	 *
	 * @throws PaymentException
	 */
	private void checkSign() throws PaymentException {
		String sign = this.makeSign();
		if (sign != this.getSign()) {
			throw new PaymentException("签名错误！");
		}
	}
	
	/**
	 * 把返回结果的XML转成Map
	 * @param xml xml数据
	 * @param interfaceType 接口类型
	 * @return
	 * @throws PaymentException
	 */
	public final Map<String, Object> init(String xml, InterfaceType interfaceType) throws PaymentException {
		// 把XML数据中结束标签和开始标签中存在的[\n]换行符替换为""
		xml = xml.replace(">\n<", "><");
		this.fromXml(xml);
		if ("SUCCESS".equalsIgnoreCase(this.valueMap.get("return_code") + "")) {
			if ("FAIL".equalsIgnoreCase(this.valueMap.get("result_code") + "")) {
				String errorMessage = "[非接口返回，文档提供] -> " + errorCode(this.valueMap.get("err_code") + "", interfaceType);
				this.valueMap.put("err_tip", errorMessage);
			}
		} else {
			if (this.isSignSet()) {
				this.checkSign();
			}
		}
		return this.getValueMap();
	}
	
	/**
	 * 错误消息，主要在对API接口提供的消息做一下字典
	 * @param errorCode 错误代码
	 * @param interfaceType 接口类型
	 * @return
	 */
	public final String errorCode(String errorCode, InterfaceType interfaceType) {
		String errorMessage = "";
		
		switch (errorCode) {
			case "INVALID_REQUEST": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：无效请求\t原因：商户系统异常导致，商户权限异常、重复请求支付、证书错误、频率限制等\t解决方案：请确认商户系统是否正常，是否具有相应支付权限，确认证书是否正确，控制频率\t支付状态：支付确认失败";
						break;
					}
					case "REVERSE": {
						errorMessage = "描述：无效请求\t原因：商户系统异常导致\t解决方案：请检查商户权限是否异常、重复请求支付、证书错误、频率限制等";
						break;
					}
					default: {
						errorMessage = "描述：参数错误\t原因：参数格式有误或者未按规则上传\t解决方案：订单重入时，要求参数值与原请求一致，请确认参数问题";
					}
				}
				break;
			}
			case "NOAUTH": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：商户无权限\t原因：商户没有开通被扫支付权限\t解决方案：请开通商户号权限。请联系产品或商务申请\t支付状态：支付确认失败";
						break;
					}
					default: {
						errorMessage = "描述：商户无此接口权限\t原因：商户未开通此接口权限\t解决方案：请商户前往申请此接口权限";
					}
				}
				break;
			}
			case "NOTENOUGH": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：余额不足\t原因：用户的零钱余额不足\t解决方案：请收银员提示用户更换当前支付的卡，然后请收银员重新扫码。建议：商户系统返回给收银台的提示为“用户余额不足.提示用户换卡支付”\t支付状态：支付确认失败";
						break;
					}
					case "REFUND": {
						errorMessage = "描述：余额不足\t原因：商户可用退款余额不足\t解决方案：此状态代表退款申请失败，商户可根据具体的错误提示做相应的处理。";
						break;
					}
					default: {
						errorMessage = "描述：余额不足\t原因：用户帐号余额不足\t解决方案：用户帐号余额不足，请用户充值或更换支付卡后再支付";
					}
				}
				break;
			}
			case "ORDERPAID": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：订单已支付\t原因：订单号重复\t解决方案：请确认该订单号是否重复支付，如果是新单，请使用新订单号提交\t支付状态：支付确认失败";
						break;
					}
					case "CLOSEORDER": {
						errorMessage = "描述：订单已支付\t原因：订单已支付，不能发起关单\t解决方案：订单已支付，不能发起关单，请当作已支付的正常交易";
						break;
					}
					default: {
						errorMessage = "描述：商户订单已支付\t原因：商户订单已支付，无需重复操作\t解决方案：商户订单已支付，无需更多操作";
					}
				}
				break;
			}
			case "ORDERCLOSED": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：订单已关闭\t原因：该订单已关\t解决方案：商户订单号异常，请重新下单支付\t支付状态：支付确认失败";
						break;
					}
					case "CLOSEORDER": {
						errorMessage = "描述：订单已关闭\t原因：订单已关闭，无法重复关闭\t解决方案：订单已关闭，无需继续调用";
						break;
					}
					default: {
						errorMessage = "描述：订单已关闭\t原因：当前订单已关闭，无法支付\t解决方案：当前订单已关闭，请重新下单";
					}
				}
				break;
			}
			case "SYSTEMERROR": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：接口返回错误\t原因：系统超时\t解决方案：请立即调用被扫订单结果查询API，查询当前订单状态，并根据订单的状态决定下一步的操作。\t支付状态：支付结果未知";
						break;
					}
					case "ORDERQUERY": {
						errorMessage = "描述：系统错误\t原因：后台系统返回错误\t解决方案：系统异常，请再调用发起查询";
						break;
					}
					case "REVERSE": {
						errorMessage = "描述：接口返回错误\t原因：系统超时\t解决方案：请立即调用被扫订单结果查询API，查询当前订单状态，并根据订单的状态决定下一步的操作。";
						break;
					}
					case "REFUND": {
						errorMessage = "描述：接口返回错误\t原因：系统超时等\t解决方案：请不要更换商户退款单号，请使用相同参数再次调用API。";
						break;
					}
					case "REFUNDQUERY": {
						errorMessage = "描述：接口返回错误\t原因：系统超时\t解决方案：请尝试再次掉调用API。";
						break;
					}
					case "CLOSEORDER": {
						errorMessage = "描述：系统错误\t原因：系统错误\t解决方案：系统异常，请重新调用该API";
						break;
					}
					default: {
						errorMessage = "描述：系统错误\t原因：系统超时\t解决方案：系统异常，请用相同参数重新调用";
					}
				}
				break;
			}
			case "APPID_NOT_EXIST": {
				errorMessage = "描述：APPID不存在\t原因：参数中缺少APPID\t解决方案：请检查APPID是否正确";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "MCHID_NOT_EXIST": {
				errorMessage = "描述：MCHID不存在\t原因：参数中缺少MCHID\t解决方案：请检查MCHID是否正确";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "APPID_MCHID_NOT_MATCH": {
				errorMessage = "描述：appid和mch_id不匹配\t原因：appid和mch_id不匹配\t解决方案：请确认appid和mch_id是否匹配";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "LACK_PARAMS": {
				errorMessage = "描述：缺少参数\t原因：缺少必要的请求参数\t解决方案：请检查参数是否齐全";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "OUT_TRADE_NO_USED": {
				errorMessage = "描述：商户订单号重复\t原因：同一笔交易不能多次提交\t解决方案：请核实商户订单号是否重复提交";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "SIGNERROR": {
				errorMessage = "描述：签名错误\t原因：参数签名结果不正确\t解决方案：请检查签名参数和方法是否都符合签名算法要求";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "XML_FORMAT_ERROR": {
				errorMessage = "描述：XML格式错误\t原因：XML格式错误\t解决方案：请检查XML参数格式是否正确";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "REQUIRE_POST_METHOD": {
				errorMessage = "描述：请使用post方法\t原因：未使用post传递参数\t解决方案：请检查请求参数是否通过post方法提交";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "POST_DATA_EMPTY": {
				errorMessage = "描述：post数据为空\t原因：post数据不能为空\t解决方案：请检查post数据是否为空";
				break;
			}
			case "NOT_UTF8": {
				errorMessage = "描述：编码格式错误\t原因：未使用指定编码格式\t解决方案：请使用UTF-8编码格式";
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "\t支付状态：支付确认失败";
						break;
					}
				}
				break;
			}
			case "PARAM_ERROR": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage += "描述：参数错误\t原因：请求参数未按指引进行填写\t解决方案：请根据接口返回的详细信息检查您的程序\t支付状态：支付确认失败";
						break;
					}
					case "REFUND":
					case "REFUNDQUERY": {
						errorMessage = "描述：参数错误\t原因：请求参数未按指引进行填写\t解决方案：请求参数错误，请重新检查再调用退款申请";
						break;
					}
					case "SENDCOUPON": {
						errorMessage = "描述：校验参数错误（会返回具体哪个参数错误）\t解决方案：根据错误提示确认参数无误并更正";
						break;
					}
					default: {
						errorMessage = "描述：参数错误\t原因：请求参数未按指引进行填写\t解决方案：请根据接口返回的详细信息检查您的程序";
					}
				}
				break;
			}
			case "AUTHCODEEXPIRE": {
				errorMessage = "描述：二维码已过期，请用户在微信上刷新后再试\t原因：用户的条码已经过期\t解决方案：请收银员提示用户，请用户在微信上刷新条码，然后请收银员重新扫码。 直接将错误展示给收银员\t支付状态：支付确认失败";
				break;
			}
			case "NOTSUPORTCARD": {
				errorMessage = "描述：不支持卡类型\t原因：用户使用卡种不支持当前支付形式\t解决方案：请用户重新选择卡种 建议：商户系统返回给收银台的提示为“该卡不支持当前支付，提示用户换卡支付或绑新卡支付”\t支付状态：支付确认失败";
				break;
			}
			case "ORDERREVERSED": {
				errorMessage = "描述：订单已撤销\t原因：当前订单已经被撤销\t解决方案：当前订单状态为“订单已撤销”，请提示用户重新支付\t支付状态：支付确认失败";
				break;
			}
			case "BANKERROR": {
				errorMessage = "描述：银行系统异常\t原因：银行端超时\t解决方案：请立即调用被扫订单结果查询API，查询当前订单的不同状态，决定下一步的操作。\t支付状态：支付结果未知";
				break;
			}
			case "USERPAYING": {
				errorMessage = "描述：用户支付中，需要输入密码\t原因：该笔交易因为业务规则要求，需要用户输入支付密码。\t解决方案：等待5秒，然后调用被扫订单结果查询API，查询当前订单的不同状态，决定下一步的操作。\t支付状态：支付结果未知";
				break;
			}
			case "AUTH_CODE_ERROR": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：授权码参数错误\t原因：请求参数未按指引进行填写\t解决方案：每个二维码仅限使用一次，请刷新再试\t支付状态：支付确认失败";
						break;
					}
					case "REVERSE": {
						errorMessage = "描述：用户支付中\t原因：用户正在支付中的订单不允许撤销\t解决方案：用户正在支付中的订单不允许撤销，请稍后再试";
						break;
					}
				}
				break;
			}
			case "AUTH_CODE_INVALID": {
				errorMessage = "描述：授权码检验错误\t原因：收银员扫描的不是微信支付的条码\t解决方案：请扫描微信支付被扫条码/二维码\t支付状态：支付确认失败";
				break;
			}
			case "BUYER_MISMATCH": {
				errorMessage = "描述：支付帐号错误\t原因：暂不支持同一笔订单更换支付方\t解决方案：请确认支付方是否相同\t支付状态：支付确认失败";
				break;
			}
			case "TRADE_ERROR": {
				switch (interfaceType.toString()) {
					case "MICROPAY": {
						errorMessage = "描述：交易错误\t原因：业务错误导致交易失败、用户账号异常、风控、规则限制等\t解决方案：请确认帐号是否存在异常\t支付状态：支付确认失败";
						break;
					}
					case "REVERSE": {
						errorMessage = "描述：订单错误\t原因：业务错误导致交易失败\t解决方案：请检查用户账号是否异常、被风控、是否符合规则限制等";
						break;
					}
				}
				break;
			}
			case "ORDERNOTEXIST": {
				switch (interfaceType.toString()) {
					case "REFUND": {
						errorMessage = "描述：订单号不存在\t原因：缺少有效的订单号\t解决方案：请检查你的订单号是否正确且是否已支付，未支付的订单不能发起退款";
						break;
					}
					default: {
						errorMessage = "描述：此交易订单号不存在\t原因：查询系统中不存在此交易订单号\t解决方案：该API只能查提交支付交易返回成功的订单，请商户检查需要查询的订单号是否正确";
					}
				}
				break;
			}
			case "INVALID_TRANSACTIONID": {
				switch (interfaceType.toString()) {
					case "REFUND":
					case "REFUNDQUERY": {
						errorMessage = "描述：无效transaction_id\t原因：请求参数未按指引进行填写\t解决方案：请求参数错误，检查原交易号是否存在或发起支付交易接口返回失败";
						break;
					}
					default: {
						errorMessage = "描述：无效transaction_id\t原因：请求参数未按指引进行填写\t解决方案：参数错误，请重新检查";
					}
				}
				break;
			}
			case "REVERSE_EXPIRE": {
				errorMessage = "描述：订单无法撤销\t原因：订单有7天的撤销有效期，过期将不能撤销\t解决方案：请检查需要撤销的订单是否超过可撤销有效期";
				break;
			}
			case "BIZERR_NEED_RETRY": {
				errorMessage = "描述：退款业务流程错误，需要商户触发重试来解决\t原因：并发情况下，业务被拒绝，商户重试即可解决\t解决方案：请不要更换商户退款单号，请使用相同参数再次调用API。";
				break;
			}
			case "TRADE_OVERDUE": {
				errorMessage = "描述：订单已经超过退款期限\t原因：订单已经超过可退款的最大期限(支付后一年内可退款)\t解决方案：请选择其他方式自行退款";
				break;
			}
			case "ERROR": {
				errorMessage = "描述：业务错误\t原因：申请退款业务发生错误\t解决方案：该错误都会返回具体的错误原因，请根据实际返回做相应处理。";
				break;
			}
			case "USER_ACCOUNT_ABNORMAL": {
				errorMessage = "描述：退款请求失败\t原因：用户帐号注销\t解决方案：此状态代表退款申请失败，商户可自行处理退款。";
				break;
			}
			case "INVALID_REQ_TOO_MUCH": {
				errorMessage = "描述：无效请求过多\t原因：连续错误请求数过多被系统短暂屏蔽\t解决方案：请检查业务是否正常，确认业务正常后请在1分钟后再来重试";
				break;
			}
			case "FREQUENCY_LIMITED": {
				errorMessage = "描述：频率限制\t原因：2个月之前的订单申请退款有频率限制\t解决方案：该笔退款未受理，请降低频率后重试";
				break;
			}
			case "REFUNDNOTEXIST": {
				errorMessage = "描述：退款订单查询失败\t原因：订单号错误或订单状态不正确\t解决方案：请检查订单号是否有误以及订单状态是否正确，如：未支付、已支付未退款";
				break;
			}
			case "STOCK_NOT_EXIST": {
				errorMessage = "描述：批次不存在\t解决方案：请检查请求参数中填写的代金券批次id和商户号是否正确";
				break;
			}
			case "BLOCKED_BY_SPAMCHK": {
				errorMessage = "描述：小号拦截\t解决方案：当前发放用户为小号，根据风险管控规则不允许给小号发放。";
				break;
			}
			case "DAY_BUDGET_NOT_ENOUGH": {
				errorMessage = "描述：批次达到单天发放预算上限\t解决方案：发券数量已经达到单天发放上限，可在商户平台批次详情页修改单天发放预算";
				break;
			}
			case "USER_AL_GET_COUPON": {
				errorMessage = "描述：你已领取过该代金券\t解决方案：用户已领过，正常逻辑报错";
				break;
			}
			case "NETWORK_ERROR": {
				errorMessage = "描述：网络环境不佳，请重试\t解决方案：请重试";
				break;
			}
			case "AL_STOCK_OVER": {
				errorMessage = "描述：活动已结束\t解决方案：活动已结束，属于正常逻辑错误";
				break;
			}
			case "FREQ_OVER_LIMIT": {
				errorMessage = "描述：超过发放频率限制，请稍后再试\t解决方案：请求对发放请求做频率控制";
				break;
			}
			case "SIGN_ERROR": {
				errorMessage = "描述：签名错误\t解决方案：验证签名有误";
				break;
			}
			case "CA_ERROR": {
				errorMessage = "描述：证书有误\t解决方案：确认证书正确，或者联系商户平台更新证书";
				break;
			}
			case "REQ_PARAM_XML_ERR": {
				errorMessage = "描述：输入参数xml格式有误\t解决方案：检查入参的xml格式是否正确";
				break;
			}
			case "COUPON_STOCK_ID_EMPTY": {
				errorMessage = "描述：批次ID为空\t解决方案：确保批次id正确传入";
				break;
			}
			case "MCH_ID_EMPTY": {
				errorMessage = "描述：商户ID为空\t解决方案：确保商户id正确传入";
				break;
			}
			case "CODE_2_ID_ERR": {
				errorMessage = "描述：商户id有误\t解决方案：检查商户id是否正确并合法";
				break;
			}
			case "OPEN_ID_EMPTY": {
				errorMessage = "描述：用户openid为空\t解决方案：检查用户openid是否正确并合法";
				break;
			}
			case "ERR_VERIFY_SSL_SERIAL": {
				errorMessage = "描述：获取客户端证书序列号失败!\t解决方案：检查证书是否正确";
				break;
			}
			case "ERR_VERIFY_SSL_SN": {
				errorMessage = "描述：获取客户端证书特征名称(DN)域失败!\t解决方案：检查证书是否正确";
				break;
			}
			case "CA_VERIFY_FAILED": {
				errorMessage = "描述：证书验证失败\t解决方案：检查证书是否正确";
				break;
			}
			case "STOCK_IS_NOT_VALID": {
				errorMessage = "描述：抱歉，该代金券已失效\t解决方案：代金券已失效，请确认代金券的有效性，重新请求";
				break;
			}
			case "STOCK_AMOUNT_NOT_ENOUGH": {
				errorMessage = "描述：代金券批次预算不足\t解决方案：商户可为该代金券批次追加预算后继续发券";
				break;
			}
			case "MCH_NO_AUTH": {
				errorMessage = "描述：商户无权限操作\t解决方案：该商户无权限进行该操作，请前往商户平台开通代金券权限";
				break;
			}
			case "COUPON_STOCK_ID_NOT_VALID": {
				errorMessage = "描述：批次id不正确\t解决方案：确认批次id正确性，以及和商户id所属关系是否正确";
				break;
			}
			case "GET_COUPON_STOCK_FAIL": {
				errorMessage = "描述：获取批次信息失败\t解决方案：确认批次id信息正确";
				break;
			}
			case "COUPON_STOCK_NOT_FOUND": {
				errorMessage = "描述：批次信息不存在\t解决方案：确认批次id信息正确";
				break;
			}
			case "OPENID_NOT_MATCH_APPID": {
				errorMessage = "描述：OPENID和APPID不匹配\t解决方案：OPENID和APPID不匹配";
				break;
			}
			case "MCHID_AND_APPID_RELATION_ERR": {
				errorMessage = "描述：校验appid与mchid关联关系错误\t解决方案：校验appid与mchid关联关系错误";
				break;
			}
			default: {
				
				errorMessage = "未知错误";
			}
		}
		return errorMessage;
	}
}
