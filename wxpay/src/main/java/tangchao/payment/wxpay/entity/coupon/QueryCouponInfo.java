package tangchao.payment.wxpay.entity.coupon;

import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Size;
import tangchao.payment.wxpay.entity.Base;

/**
 * Created by 唐潮(johnny)
 * 2018/12/29 11:03
 */
public class QueryCouponInfo extends Base {
	
	/**
	 * 代金券id
	 */
	@NotBlank(message = "[coupon_id]代金券id不能为空")
	private String couponId;
	
	/**
	 * 用户openid
	 */
	@NotBlank(message = "[openid]用户openid不能为空")
	private String openId;
	
	/**
	 * 公众账号ID
	 */
	@NotBlank(message = "[appid]公众账号ID不能为空")
	@Size(max = 32, message = "[appid]公众账号ID最大长度为32个字符")
	private String appId;
	
	/**
	 * 商户号
	 */
	@NotBlank(message = "[mch_id]商户号不能为空")
	@Size(max = 32, message = "[mch_id]商户号最大长度为32个字符")
	private String mchId;
	
	/**
	 * 批次号
	 */
	@NotBlank(message = "[stock_id]批次号不能为空")
	@Size(max = 32, message = "[stock_id]批次号最大长度为32个字符")
	private String stockId;
	
	/**
	 * 操作员
	 */
	@Size(max = 32, message = "[op_user_id]操作员最大长度为32个字符")
	private String opUserId;
	
	/**
	 * 设备号
	 */
	@Size(max = 32, message = "[device_info]设备号最大长度为32个字符")
	private String deviceInfo;
	
	/**
	 * 协议版本
	 */
	@Size(max = 32, message = "[version]协议版本最大长度为32个字符")
	private String version;
	
	/**
	 * 协议类型
	 */
	@Size(max = 32, message = "[type]协议类型最大长度为32个字符")
	private String type;
	
	public String getCouponId() {
		return couponId;
	}
	
	public void setCouponId(String couponId) {
		this.couponId = couponId;
		this.valueMap.put("coupon_id", couponId);
	}
	
	public String getOpenId() {
		return openId;
	}
	
	public void setOpenId(String openId) {
		this.openId = openId;
		this.valueMap.put("openid", openId);
	}
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		this.valueMap.put("appid", appId);
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
		this.valueMap.put("mch_id", mchId);
	}
	
	public String getStockId() {
		return stockId;
	}
	
	public void setStockId(String stockId) {
		this.stockId = stockId;
		this.valueMap.put("stock_id", stockId);
	}
	
	public String getOpUserId() {
		return opUserId;
	}
	
	public void setOpUserId(String opUserId) {
		this.opUserId = opUserId;
		this.valueMap.put("op_user_id", opUserId);
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
		this.valueMap.put("device_info", deviceInfo);
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
		this.valueMap.put("version", version);
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
		this.valueMap.put("type", type);
	}
}
