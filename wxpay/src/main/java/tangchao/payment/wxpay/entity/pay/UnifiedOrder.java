package tangchao.payment.wxpay.entity.pay;

import tangchao.payment.annotation.Min;
import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Size;
import tangchao.payment.wxpay.entity.Base;
import tangchao.payment.wxpay.enumerate.TradeType;

/**
 * Created by 唐潮(johnny)
 * 2018/11/28 17:58
 */
public class UnifiedOrder extends Base {
	
	/**
	 * 公众账号ID
	 */
	@NotBlank(message = "[appid]公众账号ID不能为空")
	@Size(max = 32, message = "[appid]公众账号ID最大长度为32个字符")
	private String appId;
	
	/**
	 * 商户号
	 */
	@NotBlank(message = "[mch_id]商户号不能为空")
	@Size(max = 32, message = "[mch_id]商户号最大长度为32个字符")
	private String mchId;
	
	/**
	 * 设备号
	 */
	@Size(max = 32, message = "[device_info]设备号最大长度为32个字符")
	private String deviceInfo;
	
	/**
	 * 商品描述
	 */
	@NotBlank(message = "[body]商品描述不能为空")
	@Size(max = 128, message = "[body]商品描述最大长度为128个字符")
	private String body;
	
	/**
	 * 商品详情
	 */
	@Size(max = 6000, message = "[detail]商品详情最大长度为6000个字符")
	private String detail;
	
	/**
	 * 附加数据
	 */
	@Size(max = 127, message = "[attach]附加数据最大长度为127个字符")
	private String attach;
	
	/**
	 * 商户订单号
	 */
	@NotBlank(message = "[out_trade_no]商户订单号不能为空")
	@Size(max = 32, message = "[out_trade_no]商户订单号最大长度为32个字符")
	private String outTradeNo;
	
	/**
	 * 标价币种
	 */
	@Size(max = 16, message = "[fee_type]标价币种最大长度为16个字符")
	private String feeType;
	
	/**
	 * 标价金额
	 */
	@NotBlank(message = "[total_fee]标价金额不能为空")
	@Min(value = 1, message = "[total_fee]标价金额最小单位为1分")
	private Integer totalFee;
	
	/**
	 * 终端IP
	 */
	@NotBlank(message = "[spbill_create_ip]终端IP不能为空")
	@Size(max = 16, message = "[spbill_create_ip]终端IP最大长度为16个字符")
	private String spbillCreateIp;
	
	/**
	 * 交易起始时间
	 */
	@Size(max = 14, message = "[time_start]交易起始时间最大长度为14个字符")
	private String timeStart;
	
	/**
	 * 交易结束时间
	 */
	@Size(max = 14, message = "[time_expire]交易结束时间最大长度为14个字符")
	private String timeExpire;
	
	/**
	 * 订单优惠标记
	 */
	@Size(max = 32, message = "[goods_tag]订单优惠标记最大长度为32个字符")
	private String goodsTag;
	
	/**
	 * 通知地址
	 */
	@NotBlank(message = "[notify_url]通知地址不能为空")
	@Size(max = 256, message = "[notify_url]通知地址最大长度为256个字符")
	private String notifyUrl;
	
	/**
	 * 交易类型
	 */
	@NotBlank(message = "[trade_type]交易类型不能为空")
	@Size(max = 16, message = "[trade_type]交易类型最大长度为16个字符")
	private String tradeType;
	
	/**
	 * 商品ID
	 */
	@Size(max = 32, message = "[product_id]商品ID最大长度为32个字符")
	private String productId;
	
	/**
	 * 指定支付方式
	 */
	@Size(max = 32, message = "[limit_pay]指定支付方式最大长度为32个字符")
	private String limitPay;
	
	/**
	 * 用户标识
	 */
	@Size(max = 128, message = "[openid]用户标识最大长度为128个字符")
	private String openId;
	
	/**
	 * 场景信息
	 */
	@Size(max = 256, message = "[scene_info]场景信息最大长度为256个字符")
	private String sceneInfo;
	@Size(max = 32, message = "[id]场景信息之门店id最大长度为32个字符")
	private String id; // 场景信息之门店id
	@Size(max = 64, message = "[name]场景信息之门店名称最大长度为64个字符")
	private String name; // 场景信息之门店名称
	@Size(max = 6, message = "[area_code]场景信息之门店行政区划码最大长度为6个字符")
	private String areaCode; // 场景信息之门店行政区划码
	@Size(max = 128, message = "[address]场景信息之门店详细地址最大长度为128个字符")
	private String address; // 场景信息之门店详细地址
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		this.valueMap.put("appid", appId);
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
		this.valueMap.put("mch_id", mchId);
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
		this.valueMap.put("device_info", deviceInfo);
	}
	
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
		this.valueMap.put("body", body);
	}
	
	public String getDetail() {
		return detail;
	}
	
	public void setDetail(String detail) {
		this.detail = detail;
		this.valueMap.put("detail", detail);
	}
	
	public String getAttach() {
		return attach;
	}
	
	public void setAttach(String attach) {
		this.attach = attach;
		this.valueMap.put("attach", attach);
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
		this.valueMap.put("out_trade_no", outTradeNo);
	}
	
	public String getFeeType() {
		return feeType;
	}
	
	public void setFeeType(String feeType) {
		this.feeType = feeType;
		this.valueMap.put("fee_type", feeType);
	}
	
	public Integer getTotalFee() {
		return totalFee;
	}
	
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
		this.valueMap.put("total_fee", totalFee);
	}
	
	public String getSpbillCreateIp() {
		return spbillCreateIp;
	}
	
	/**
	 * 当
	 * @param spbillCreateIp
	 */
	public void setSpbillCreateIp(String spbillCreateIp) {
		this.spbillCreateIp = spbillCreateIp;
		this.valueMap.put("spbill_create_ip", spbillCreateIp);
	}
	
	public String getTimeStart() {
		return timeStart;
	}
	
	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
		this.valueMap.put("time_start", timeStart);
	}
	
	public String getTimeExpire() {
		return timeExpire;
	}
	
	public void setTimeExpire(String timeExpire) {
		this.timeExpire = timeExpire;
		this.valueMap.put("time_expire", timeExpire);
	}
	
	public String getGoodsTag() {
		return goodsTag;
	}
	
	public void setGoodsTag(String goodsTag) {
		this.goodsTag = goodsTag;
		this.valueMap.put("goods_tag", goodsTag);
	}
	
	public String getNotifyUrl() {
		return notifyUrl;
	}
	
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
		this.valueMap.put("notify_url", notifyUrl);
	}
	
	public String getTradeType() {
		return tradeType;
	}
	
	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType.toString();
		this.valueMap.put("trade_type", tradeType.toString());
	}
	
	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
		this.valueMap.put("product_id", productId);
	}
	
	public String getLimitPay() {
		return limitPay;
	}
	
	public void setLimitPay(String limitPay) {
		this.limitPay = limitPay;
		this.valueMap.put("limit_pay", limitPay);
	}
	
	public String getOpenId() {
		return openId;
	}
	
	public void setOpenId(String openId) {
		this.openId = openId;
		this.valueMap.put("openid", openId);
	}
	
	public String getSceneInfo() {
		return sceneInfo;
	}
	
	/**
	 * 设置场景信息
	 *
	 * @param id       门店id
	 * @param name     门店名称
	 * @param areaCode 门店行政区划码
	 * @param address  门店详细地址
	 */
	public void setSceneInfo(String id, String name, String areaCode, String address) {
		this.id = id;
		this.name = name;
		this.areaCode = areaCode;
		this.address = address;
		String sceneInfo = "{\"store_info\":{" + "\"id\":\"" + id + "\"," +
				"\"name\":\"" + name + "\"," +
				"\"area_code\":\"" + areaCode + "\"," +
				"\"address\":\"" + address + "\"}}";
		this.sceneInfo = sceneInfo;
		this.valueMap.put("scene_info", sceneInfo);
	}
	
	/**
	 * 设置IOS端的H5支付场景信息
	 *
	 * @param appName 应用名
	 * @param bundleId 束ID
	 */
	public void setSceneInfoH5IOS(String appName, String bundleId) {
		String sceneInfo = "{\"h5_info\":{\"type\":\"IOS\",\"app_name\":\"" + appName + "\",\"bundle_id\":\"" + bundleId + "\"}}";
		this.sceneInfo = sceneInfo;
		this.valueMap.put("scene_info", sceneInfo);
	}
	
	/**
	 * 设置Android端的H5支付场景信息
	 *
	 * @param appName 应用名
	 * @param packageName 包名
	 */
	public void setSceneInfoH5Android(String appName, String packageName) {
		String sceneInfo = "{\"h5_info\":{\"type\":\"Android\",\"app_name\":\"" + appName + "\",\"package_name\":\"" + packageName + "\"}}";
		this.sceneInfo = sceneInfo;
		this.valueMap.put("scene_info", sceneInfo);
	}
	
	/**
	 * 设置Wap端的H5支付场景信息
	 * @param wapName WAP网站名
	 * @param wapUrl WAP网站URL地址
	 */
	public void setSceneInfoH5Wap(String wapName, String wapUrl) {
		String sceneInfo = "{\"h5_info\":{\"type\":\"Wap\",\"wap_name\": \"" + wapName + "\",\"wap_url\":\"" + wapUrl + "\"}}";
		this.sceneInfo = sceneInfo;
		this.valueMap.put("scene_info", sceneInfo);
	}
}
