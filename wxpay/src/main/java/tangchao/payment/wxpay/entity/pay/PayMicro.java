package tangchao.payment.wxpay.entity.pay;


import tangchao.payment.annotation.Min;
import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Pattern;
import tangchao.payment.annotation.Size;
import tangchao.payment.wxpay.entity.Base;

/**
 * Created by 唐潮(johnny)
 * 2018/11/27 16:37
 */
public class PayMicro extends Base {
	/**
	 * 公众账号ID
	 */
	@NotBlank(message = "[appid]公众帐号ID不能为空")
	@Size(max = 32, message = "[appid]公众帐号ID最大长度为32个字符")
	private String appId;
	
	/**
	 * 商户号
	 */
	@NotBlank(message = "[mch_id]商户号不能为空")
	@Size(max = 32, message = "[mch_id]商户号最大长度为32个字符")
	private String mchId;
	
	/**
	 * 设备号
	 */
	@Size(max = 32, message = "[device_info]设备号最大长度为32个字符")
	private String deviceInfo;
	
	/**
	 * 商品描述
	 */
	@NotBlank(message = "[body]商品描述不能为空")
	@Size(max = 128, message = "[body]商品描述最大长度为128个字符")
	private String body;
	
	/**
	 * 商品详情
	 */
	@Size(max = 6000, message = "[detail]商品详情最大长度为6000个字符")
	private String detail;
	
	/**
	 * 附加数据
	 */
	@Size(max = 127, message = "[attach]附加数据最大长度为127个字符")
	private String attach;
	
	/**
	 * 商户订单号
	 */
	@NotBlank(message = "[out_trade_no]商户订单号不能为空")
	@Size(max = 32, message = "[out_trade_no]商户订单号最大长度为127个字符")
	private String outTradeNo;
	
	/**
	 * 订单金额
	 */
	@NotBlank(message = "[total_fee]订单金额不能为空")
	@Min(value = 1, message = "[total_fee]订单金额最小单位为1分")
	private Integer totalFee;
	
	/**
	 * 货币类型
	 */
	@Size(max = 16, message = "[fee_type]货币类型最大长度为16个字符")
	private String feeType;
	
	@NotBlank(message = "[spbill_create_ip]终端IP不能为空")
	@Size(max = 16, message = "[spbill_create_ip]终端IP最大长度为16个字符")
	private String spbillCreateIp;
	
	/**
	 * 订单优惠标记
	 */
	@Size(max = 32, message = "[goods_tag]订单优惠标记最大长度为32个字符")
	private String goodsTag;
	
	/**
	 * 指定支付方式
	 */
	@Size(max = 32, message = "[limit_pay]指定支付方式最大长度为32个字符")
	private String limitPay;
	
	/**
	 * 交易起始时间
	 */
	@Size(max = 14, message = "[time_start]交易起始时间最大长度为14个字符")
	public String timeStart;
	
	/**
	 * 交易结束时间
	 */
	@Size(max = 14, message = "[time_expire]交易结束时间最大长度为14个字符")
	private String timeExpire;
	
	/**
	 * 授权码
	 */
	@NotBlank(message = "[auth_code]授权码不能为空")
	@Size(max = 128, message = "[auth_code]授权码最大长度为128个字符")
	@Pattern(regexp = "^1[0|1|2|3|4|5]\\d{16}$", message = "[auth_code]授权码格式错误, 应为18位纯数字，以10、11、12、13、14、15开头")
	private String authCode;
	
	/**
	 * 场景信息
	 */
	@Size(max = 256, message = "[scene_info]场景信息最大长度为256个字符")
	private String sceneInfo;
	@Size(max = 32, message = "[id]场景信息之门店id最大长度为32个字符")
	private String id; // 场景信息之门店id
	@Size(max = 64, message = "[name]场景信息之门店名称最大长度为64个字符")
	private String name; // 场景信息之门店名称
	@Size(max = 6, message = "[area_code]场景信息之门店行政区划码最大长度为6个字符")
	private String areaCode; // 场景信息之门店行政区划码
	@Size(max = 128, message = "[address]场景信息之门店详细地址最大长度为128个字符")
	private String address; // 场景信息之门店详细地址
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		this.valueMap.put("appid", appId);
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
		this.valueMap.put("mch_id", mchId);
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
		this.valueMap.put("device_info", deviceInfo);
	}
	
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
		this.valueMap.put("body", body);
	}
	
	public String getDetail() {
		return detail;
	}
	
	public void setDetail(String detail) {
		this.detail = detail;
		this.valueMap.put("detail", detail);
	}
	
	public String getAttach() {
		return attach;
	}
	
	public void setAttach(String attach) {
		this.attach = attach;
		this.valueMap.put("attach", attach);
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
		this.valueMap.put("out_trade_no", outTradeNo);
	}
	
	public Integer getTotalFee() {
		return totalFee;
	}
	
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
		this.valueMap.put("total_fee", totalFee);
	}
	
	public String getFeeType() {
		return feeType;
	}
	
	public void setFeeType(String feeType) {
		this.feeType = feeType;
		this.valueMap.put("fee_type", feeType);
	}
	
	public String getSpbillCreateIp() {
		return spbillCreateIp;
	}
	
	public void setSpbillCreateIp(String spbillCreateIp) {
		this.spbillCreateIp = spbillCreateIp;
		this.valueMap.put("spbill_create_ip", spbillCreateIp);
	}
	
	public String getGoodsTag() {
		return goodsTag;
	}
	
	public void setGoodsTag(String goodsTag) {
		this.goodsTag = goodsTag;
		this.valueMap.put("goods_tag", goodsTag);
	}
	
	public String getLimitPay() {
		return limitPay;
	}
	
	public void setLimitPay(String limitPay) {
		this.limitPay = limitPay;
		this.valueMap.put("limit_pay", limitPay);
	}
	
	public String getTimeStart() {
		return timeStart;
	}
	
	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
		this.valueMap.put("time_start", timeStart);
	}
	
	public String getTimeExpire() {
		return timeExpire;
	}
	
	public void setTimeExpire(String timeExpire) {
		this.timeExpire = timeExpire;
		this.valueMap.put("time_expire", timeExpire);
	}
	
	public String getAuthCode() {
		return authCode;
	}
	
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
		this.valueMap.put("auth_code", authCode);
	}
	
	public String getSceneInfo() {
		return sceneInfo;
	}
	
	/**
	 * 设计场景信息
	 *
	 * @param id       门店id
	 * @param name     门店名称
	 * @param areaCode 门店行政区划码
	 * @param address  门店详细地址
	 */
	public void setSceneInfo(String id, String name, String areaCode, String address) {
		this.id = id;
		this.name = name;
		this.areaCode = areaCode;
		this.address = address;
		String sceneInfo = "{\"store_info\":{" + "\"id\":\"" + id + "\"," +
				"\"name\":\"" + name + "\"," +
				"\"area_code\":\"" + areaCode + "\"," +
				"\"address\":\"" + address + "\"}}";
		this.sceneInfo = sceneInfo;
		this.valueMap.put("scene_info", sceneInfo);
	}
}
