package tangchao.payment.wxpay.entity.pay;

import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Size;
import tangchao.payment.wxpay.entity.Base;

/**
 * Created by 唐潮(johnny)
 * 2018/11/28 17:12
 */
public class RefundQuery extends Base {
	
	/**
	 * 公众账号ID
	 */
	@NotBlank(message = "[appid]公众账号ID不能为空")
	@Size(max = 32, message = "[appid]公众账号ID最大长度为32个字符")
	private String appId;
	
	/**
	 * 商户号
	 */
	@NotBlank(message = "[mch_id]商户号不能为空")
	@Size(max = 32, message = "[mch_id]商户号最大长度为32个字符")
	private String mchId;
	
	/**
	 * 微信订单号
	 */
	@Size(max = 32, message = "[transaction_id]微信订单号最大长度为32个字符")
	private String transactionId;
	
	/**
	 * 商户订单号
	 */
	@Size(max = 32, message = "[out_trade_no]商户订单号最大长度为32个字符")
	private String outTradeNo;
	
	/**
	 * 商户退款单号
	 */
	@Size(max = 64, message = "[out_refund_no]商户退款单号最大长度为64个字符")
	private String outRefundNo;
	
	/**
	 * 微信退款单号
	 */
	@Size(max = 32, message = "[refund_id]微信退款单号最大长度为32个字符")
	private String refundId;
	
	/**
	 * 偏移量
	 */
	private Integer offset;
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		this.valueMap.put("appid", appId);
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
		this.valueMap.put("mch_id", mchId);
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
		this.valueMap.put("transaction_id", transactionId);
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
		this.valueMap.put("out_trade_no", outTradeNo);
	}
	
	public String getOutRefundNo() {
		return outRefundNo;
	}
	
	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
		this.valueMap.put("out_refund_no", outRefundNo);
	}
	
	public String getRefundId() {
		return refundId;
	}
	
	public void setRefundId(String refundId) {
		this.refundId = refundId;
		this.valueMap.put("refund_id", refundId);
	}
	
	public Integer getOffset() {
		return offset;
	}
	
	public void setOffset(Integer offset) {
		this.offset = offset;
		this.valueMap.put("offset", offset);
	}
}
