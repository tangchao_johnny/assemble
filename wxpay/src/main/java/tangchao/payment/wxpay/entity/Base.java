package tangchao.payment.wxpay.entity;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tangchao.payment.annotation.NotBlank;
import tangchao.payment.annotation.Size;
import tangchao.payment.common.util.MD5Util;
import tangchao.payment.common.util.XmlUtil;
import tangchao.payment.exception.PaymentException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by 唐潮(johnny)
 * 2018/11/27 14:27
 */
public class Base {
	
	protected SortedMap<String, Object> valueMap = new TreeMap<String, Object>();
	
	/**
	 * 签名，详见签名生成算法
	 */
	@Size(max = 32, message = "[sign]签名最大长度为32位")
	private String sign;
	
	/**
	 * 签名类型
	 */
	@Size(max = 32, message = "[sign_type]签名类型最大长度为32位")
	private String signType;
	
	/**
	 * 随机字符串
	 */
	@NotBlank(message = "[nonce_str]随机字符串不能为空")
	@Size(max = 32, message = "[nonce_str]随机字符串最大长度为32个字符")
	private String nonceStr;
	
	/**
	 * 签名用到的Key
	 */
	@NotBlank(message = "[key]微信支付签名KEY不能为空")
	private String key;
	
	public String getSign() {
		if (sign == null && this.valueMap.get("sign") != null) {
			sign = this.valueMap.get("sign").toString();
		}
		return sign;
	}
	
	public void setSign() {
		String sign = this.makeSign();
		this.sign = sign;
		this.valueMap.put("sign", sign);
	}
	
	public boolean isSignSet() {
		return this.valueMap.get("sign") != null;
	}
	
	public String getSignType() {
		return signType;
	}
	
	public void setSignType(String signType) {
		this.signType = signType;
		this.valueMap.put("sign_type", signType);
	}
	
	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
		this.valueMap.put("nonce_str", nonceStr);
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String makeSign() {
		StringBuffer sb = new StringBuffer();
		Set<Map.Entry<String, Object>> set = this.valueMap.entrySet();
		Iterator<Map.Entry<String, Object>> it = set.iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			String k = entry.getKey();
			Object v = entry.getValue();
			if (null != v && !"".equals(v) && !"sign".equals(k) && !"key".equals(k)) {
				sb.append(k + "=" + v + "&");
			}
		}
		sb.append("key=" + this.key);
		String sign = MD5Util.MD5Encode(sb.toString()).toUpperCase();
		return sign;
	}
	
	/**
	 * 把Map数据集转XML
	 *
	 * @return
	 * @throws PaymentException
	 */
	public String toXml() throws PaymentException {
		if (this.valueMap.size() == 0) {
			throw new PaymentException("转成XML数据的数据集异常！");
		}
		return XmlUtil.mapToXml(this.valueMap);
	}
	
	/**
	 * 把XML转Map数据集
	 *
	 * @param xml
	 * @throws PaymentException
	 */
	public void fromXml(String xml) throws PaymentException {
		try {
			// 定义工厂API 使应用程序能够从XML文档获取生成DOM对象树的解析器
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			// 获取此类的实例之后，将可以从各种输入源解析XML
			DocumentBuilder builder = factory.newDocumentBuilder();
			// Document接口表示整个HTML或XML文档，从概念上讲，它是文档树的根，并提供对文档数据的基本访问
			org.w3c.dom.Document document = builder.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
			// 获取根节点
			org.w3c.dom.Element root = document.getDocumentElement();
			//读取database节点NodeList接口提供对节点的有序集合的抽象
			NodeList nodeList = root.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				// 获取一个节点
				Node node = nodeList.item(i);
				this.valueMap.put(node.getNodeName(), node.getTextContent());
			}
		} catch (ParserConfigurationException pce) {
			throw new PaymentException(pce);
		} catch (SAXException saxe) {
			throw new PaymentException(saxe);
		} catch (IOException ioe) {
			throw new PaymentException(ioe);
		}
	}
	
	public SortedMap<String, Object> getValueMap() {
		return this.valueMap;
	}
}
