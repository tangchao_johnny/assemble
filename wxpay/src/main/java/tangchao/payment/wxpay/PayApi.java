package tangchao.payment.wxpay;

import tangchao.payment.exception.PaymentException;
import tangchao.payment.wxpay.entity.pay.*;
import tangchao.payment.wxpay.enumerate.InterfaceType;
import tangchao.payment.wxpay.enumerate.ResultType;
import tangchao.payment.wxpay.enumerate.TradeType;

/**
 * Created by 唐潮(johnny)
 * 2018/11/27 16:34
 */
public class PayApi extends Pay {
	
	/**
	 * 提交付款码支付
	 *
	 * @param payMicroPay 提交付款码支付参数
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object micropay(PayMicro payMicroPay, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/pay/micropay";
		payMicroPay.setNonceStr(getNonceStr());
		checkObj(payMicroPay);
		// 对参数进行签名
		payMicroPay.setSign();
		return request(url, payMicroPay.toXml(), InterfaceType.MICROPAY, resultType);
	}
	
	/**
	 * 查询订单
	 *
	 * @param payOrderQuery 查询订单参数
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object orderquery(OrderQuery payOrderQuery, ResultType resultType) throws PaymentException {
		if ((payOrderQuery.getTransactionId() == null || payOrderQuery.getTransactionId().length() == 0) &&
				(payOrderQuery.getOutTradeNo() == null || payOrderQuery.getOutTradeNo().length() == 0)) {
			throw new PaymentException("[out_trade_no]商户订单号、[transaction_id]微信订单号至少填一个！");
		}
		String url = "https://api.mch.weixin.qq.com/pay/orderquery";
		payOrderQuery.setNonceStr(getNonceStr());
		checkObj(payOrderQuery);
		// 对参数进行签名
		payOrderQuery.setSign();
		return request(url, payOrderQuery.toXml(), InterfaceType.ORDERQUERY, resultType);
	}
	
	/**
	 * 撤销订单(需要证书)
	 *
	 * @param payReverse      撤销订单参数
	 * @param certificatePath 证书全路径(微信支付的.p12证书)
	 * @param certificateKey  证书密钥，如果为空则取mch_id
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object reverse(Reverse payReverse, String certificatePath, String certificateKey, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/secapi/pay/reverse";
		payReverse.setNonceStr(getNonceStr());
		checkObj(payReverse);
		// 对参数进行签名
		payReverse.setSign();
		certificateKey = (certificateKey == null && certificateKey.length() == 0) ? payReverse.getMchId() : certificateKey;
		return requestCertificate(url, payReverse.toXml(), certificatePath, certificateKey, InterfaceType.REVERSE, resultType);
	}
	
	/**
	 * 申请退款(需要证书)
	 *
	 * @param payRefund       申请退款参数
	 * @param certificatePath 证书全路径(微信支付的.p12证书)
	 * @param certificateKey  证书密钥，如果为空则取mch_id
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object refund(Refund payRefund, String certificatePath, String certificateKey, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/secapi/pay/refund";
		payRefund.setNonceStr(getNonceStr());
		if ((payRefund.getTransactionId() == null || payRefund.getTransactionId().length() == 0) &&
				(payRefund.getOutTradeNo() == null || payRefund.getOutTradeNo().length() == 0)) {
			throw new PaymentException("[out_trade_no]商户订单号、[transaction_id]微信订单号至少填一个！");
		}
		checkObj(payRefund);
		// 对参数进行签名
		payRefund.setSign();
		certificateKey = (certificateKey == null && certificateKey.length() == 0) ? payRefund.getMchId() : certificateKey;
		return requestCertificate(url, payRefund.toXml(), certificatePath, certificateKey, InterfaceType.REFUND, resultType);
	}
	
	/**
	 * 查询退款
	 *
	 * @param payRefundQuery 查询退款参数
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object refundquery(RefundQuery payRefundQuery, ResultType resultType) throws PaymentException {
		if ((payRefundQuery.getTransactionId() == null || payRefundQuery.getTransactionId().length() == 0) &&
				(payRefundQuery.getOutTradeNo() == null || payRefundQuery.getOutTradeNo().length() == 0) &&
				(payRefundQuery.getOutRefundNo() == null || payRefundQuery.getOutRefundNo().length() == 0) &&
				(payRefundQuery.getRefundId() == null || payRefundQuery.getRefundId().length() == 0)) {
			throw new PaymentException("[out_trade_no]商户订单号、[transaction_id]微信订单号、[out_refund_no]商户退款单号、[refund_id]微信退款单号至少填一个！");
		}
		String url = "https://api.mch.weixin.qq.com/pay/refundquery";
		payRefundQuery.setNonceStr(getNonceStr());
		checkObj(payRefundQuery);
		// 对参数进行签名
		payRefundQuery.setSign();
		return request(url, payRefundQuery.toXml(), InterfaceType.REFUNDQUERY, resultType);
	}
	
	/**
	 * 统一下单
	 *
	 * @param payUnifiedOrder 统一下单参数
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object unifiedorder(UnifiedOrder payUnifiedOrder, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
		if (payUnifiedOrder.getTradeType().equals(TradeType.JSAPI.toString()) && (payUnifiedOrder.getOpenId() == null || payUnifiedOrder.getOpenId().length() == 0)) {
			throw new PaymentException("trade_type=JSAPI时（即JSAPI支付），[openid]用户标识必传，[openid]用户标识为微信用户在商户对应appid下的唯一标识；详情见开发者文档。");
		}
		if (payUnifiedOrder.getTradeType().equals(TradeType.NATIVE.toString()) && (payUnifiedOrder.getProductId() == null || payUnifiedOrder.getProductId().length() == 0)) {
			throw new PaymentException("trade_type=NATIVE时，[product_id]商品ID必传，[product_id]用户标识为二维码中包含的商品ID，商户自行定义；详情见开发者文档。");
		}
		if (payUnifiedOrder.getTradeType().equals(TradeType.MWEB.toString()) && (payUnifiedOrder.getSceneInfo() == null || payUnifiedOrder.getSceneInfo().length() == 0)) {
			throw new PaymentException("trade_type=MWEB时，[scene_info]场景信息必传，[scene_info]场景信息用于上报支付的场景信息,针对H5支付有[IOS/Android/Wap]三种场景,请根据提供的方法[setSceneInfoH5*]对应场景设置相应的值进行上报,H5支付不建议在APP端使用，不然可能会出现兼容性问题；详情见开发者文档。");
		}
		payUnifiedOrder.setNonceStr(getNonceStr());
		checkObj(payUnifiedOrder);
		// 对参数进行签名
		payUnifiedOrder.setSign();
		return request(url, payUnifiedOrder.toXml(), InterfaceType.NULL, resultType);
	}
	
	/**
	 * 关闭订单
	 *
	 * @param payCloseOrder 关闭订单参数
	 * @param resultType      结果类型，默认为JSON，支持三种格式[JSON=>String，XML=>String，MAP=>Map<String, Object>]，收到结果请做类型转换
	 * @return
	 * @throws PaymentException
	 */
	public static final Object closeorder(CloseOrder payCloseOrder, ResultType resultType) throws PaymentException {
		String url = "https://api.mch.weixin.qq.com/pay/closeorder";
		payCloseOrder.setNonceStr(getNonceStr());
		checkObj(payCloseOrder);
		// 对参数进行签名
		payCloseOrder.setSign();
		return request(url, payCloseOrder.toXml(), InterfaceType.CLOSEORDER, resultType);
	}
}
