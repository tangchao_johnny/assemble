package tangchao.payment.wxpay;

import tangchao.payment.check.util.AnnotaionUtils;
import tangchao.payment.common.util.HttpUtil;
import tangchao.payment.common.util.JsonUtil;
import tangchao.payment.common.util.XmlUtil;
import tangchao.payment.exception.PaymentException;
import tangchao.payment.wxpay.entity.pay.Result;
import tangchao.payment.wxpay.enumerate.InterfaceType;
import tangchao.payment.wxpay.enumerate.ResultType;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by 唐潮(johnny)
 * 2018/11/27 17:49
 */
public class Pay {
	
	/**
	 * 对请求参数的内容做检验
	 *
	 * @param obj 参数对象
	 * @param <T>
	 * @throws PaymentException
	 */
	protected static final <T> void checkObj(T obj) throws PaymentException {
		final List<String> validList = AnnotaionUtils.check(obj);
		if (validList.size() > 0) {
			final StringBuffer message = new StringBuffer();
			for (String valid : validList) {
				message.append(valid).append("\n");
			}
			throw new PaymentException(message.toString());
		}
	}
	
	/**
	 * 封装普通的POST请求
	 *
	 * @param url           请求地址
	 * @param xml           请求XML数据
	 * @param interfaceType 接口类型
	 * @param resultType    结果类型
	 * @return
	 * @throws PaymentException
	 */
	protected static final Object request(String url, String xml, InterfaceType interfaceType, ResultType resultType) throws PaymentException {
		String response = HttpUtil.httpPost(url, xml);
		final Result payResult = new Result();
		final Map<String, Object> result = payResult.init(response, interfaceType);
		return conversionResult(result, resultType);
	}
	
	/**
	 * 封装带证书的POST请求
	 *
	 * @param url             请求地址
	 * @param xml             请求XML数据
	 * @param certificatePath 证书地址
	 * @param certificateKey  证书密钥
	 * @param interfaceType   接口类型
	 * @param resultType      结果类型
	 * @return
	 * @throws PaymentException
	 */
	protected static final Object requestCertificate(final String url, final String xml, final String certificatePath, final String certificateKey, final InterfaceType interfaceType, ResultType resultType) throws PaymentException {
		final String response = HttpUtil.httpPostCertificate(url, xml, certificateKey, certificatePath);
		final Result payResult = new Result();
		final Map<String, Object> result = payResult.init(response, interfaceType);
		return conversionResult(result, resultType);
	}
	
	private static final Object conversionResult(final Map<String, Object> result, ResultType resultType) throws PaymentException {
		if (resultType == null) {
			resultType = ResultType.JSON;
		}
		
		if (resultType.toString().equals("MAP")) {
			return result;
		} else if (resultType.toString().equals("XML")) {
			return XmlUtil.mapToXml(result);
		} else {
			return JsonUtil.mapToJson(result);
		}
	}
	
	/**
	 * 产生随机字符串，不长于32位
	 *
	 * @return 产生的随机字符串
	 */
	public static final String getNonceStr() {
		String nonceStr = UUID.randomUUID().toString().replace("-", "");
		if (nonceStr.length() > 32) {
			nonceStr = nonceStr.substring(0, 32);
		}
		return nonceStr;
	}
}
