package tangchao.payment.wxpay.enumerate;

/**
 * Created by 唐潮(johnny)
 * 2018/11/28 18:31
 */
public enum TradeType {
	JSAPI,
	NATIVE,
	APP,
	MWEB
}
