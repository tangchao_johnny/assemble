package tangchao.payment.wxpay.enumerate;

/**
 * Created by 唐潮(johnny)
 * 2018/11/29 15:28
 */
public enum InterfaceType {
	MICROPAY,
	ORDERQUERY,
	REVERSE,
	REFUND,
	REFUNDQUERY,
	CLOSEORDER,
	SENDCOUPON,
	QUERYCOUPONSTOCK,
	QUERYCOUPONINFO,
	NULL
}
