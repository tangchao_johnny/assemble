package tangchao.payment.wxpay.enumerate;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 11:32
 */
public enum ResultType {
	MAP,
	JSON,
	XML
}
