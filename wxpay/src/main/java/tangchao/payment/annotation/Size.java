package tangchao.payment.annotation;

import java.lang.annotation.*;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 15:37
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Size {
	String message() default "字符长度验证";
	
	int min() default 0;
	
	int max() default 2147483647;
}
