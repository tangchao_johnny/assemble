package tangchao.payment.annotation;

import java.lang.annotation.*;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 15:08
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface NotBlank {
	String message() default "不能为空验证";
}
