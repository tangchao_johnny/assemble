package tangchao.payment.annotation;

import java.lang.annotation.*;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 16:50
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Max {
	String message() default "最大值验证";
	
	long value();
}
