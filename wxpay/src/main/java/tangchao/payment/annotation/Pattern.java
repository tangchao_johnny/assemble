package tangchao.payment.annotation;

import java.lang.annotation.*;

/**
 * Created by 唐潮(johnny)
 * 2018/11/30 16:50
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Pattern {
	String message() default "请按正则表达式验证";
	
	String regexp();
}
