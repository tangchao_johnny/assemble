package tangchao.wxpublic.exception;

/**
 * Created by 唐潮(johnny)
 * 2018/12/3 14:30
 */
public class WxpublicException extends Exception {
	
	//无参构造方法
	public WxpublicException() {
		super();
	}
	
	//有参的构造方法
	public WxpublicException(String message) {
		super(message);
	}
	
	// 用指定的详细信息和原因构造一个新的异常
	public WxpublicException(String message, Throwable cause) {
		super(message, cause);
	}
	
	//用指定原因构造一个新的异常
	public WxpublicException(Throwable cause) {
		super(cause);
	}
	
}
