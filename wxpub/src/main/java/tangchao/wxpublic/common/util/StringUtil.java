package tangchao.wxpublic.common.util;

import java.util.UUID;

/**
 * Created by 唐潮(johnny)
 * 2018/12/3 14:42
 */
public class StringUtil {
	
	/**
	 * 获取UUID
	 *
	 * @param delSpe
	 * @return
	 */
	public static String getUuid(boolean delSpe) {
		String uuid = UUID.randomUUID().toString();
		if (delSpe) {
			uuid = uuid.replace("-", "");
		}
		return uuid;
	}
	
	/**
	 * 获取时间戳
	 *
	 * @return
	 */
	public static String createTimestamp() {
		return Long.toString(System.currentTimeMillis() / 1000);
	}
}
