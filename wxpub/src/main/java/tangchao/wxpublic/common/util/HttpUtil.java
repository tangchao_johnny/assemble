package tangchao.wxpublic.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import tangchao.wxpublic.exception.WxpublicException;

public class HttpUtil {
	
	public static void responseWithJson(HttpServletResponse response, String jsonStr) throws WxpublicException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.append(jsonStr);
		} catch (IOException ioe) {
			throw new WxpublicException(ioe);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	public static void responseWithJson(HttpServletResponse response, Map<String, Object> map) throws WxpublicException {
		responseWithJson(response, JSON.toJSONString(map, SerializerFeature.WriteMapNullValue));
	}
	
	public static void responseObjectWithJson(HttpServletResponse response, Object object) throws WxpublicException {
		responseWithJson(response, JSON.toJSONString(object, SerializerFeature.WriteMapNullValue));
	}

	/**
	 * 安全的SSL POST请求
	 * @param url
	 * @param params
	 * @return
	 * @throws WxpublicException
	 */
	public static String sendPostSSL(String url, String params) throws WxpublicException {
		try {
			StringBuffer sb = new StringBuffer();
			HttpsURLConnection httpsURLConnection = getRemoteAccessSSLConn(url);
			httpsURLConnection.setRequestMethod("POST");
			httpsURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			
			OutputStreamWriter osw = new OutputStreamWriter(httpsURLConnection.getOutputStream(), "UTF-8");
			osw.write(params);
			osw.flush();
			// 获得响应状态
			int resultCode = httpsURLConnection.getResponseCode();
			if (HttpURLConnection.HTTP_OK == resultCode) {
				int charCountin = -1;
				InputStream in = httpsURLConnection.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
				while ((charCountin = br.read()) != -1) {
					sb.append((char) charCountin);
				}
				in.close();
			}
			osw.close();
			return sb.toString();
		} catch (NoSuchAlgorithmException nsae) {
			throw new WxpublicException(nsae);
		} catch (KeyManagementException kme) {
			throw new WxpublicException(kme);
		} catch (IOException ioe) {
			throw new WxpublicException(ioe);
		}
	}
	
	/**
	 * 普通的POST请求
	 * @param url
	 * @param params
	 * @return
	 * @throws WxpublicException
	 */
	public static String sendPost(String url, String params) throws WxpublicException {
		try {
			StringBuffer sb = new StringBuffer();
			HttpURLConnection httpConnection = (HttpURLConnection) getRemoteAccessConn(url);
			
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			OutputStreamWriter osw = new OutputStreamWriter(httpConnection.getOutputStream(), "UTF-8");
			osw.write(params);
			osw.flush();
			// 获得响应状态
			int resultCode = httpConnection.getResponseCode();
			if (HttpURLConnection.HTTP_OK == resultCode) {
				int charCount = -1;
				InputStream in = httpConnection.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
				while ((charCount = br.read()) != -1) {
					sb.append((char) charCount);
				}
				in.close();
			}
			osw.close();
			return sb.toString();
		} catch (IOException ioe) {
			throw new WxpublicException(ioe);
		}
	}

	/**
	 * 发送安全的SSL GET请求
	 * @param url
	 * @return
	 * @throws WxpublicException
	 */
	public static String sendGetSSL(String url) throws WxpublicException {
		try {
			StringBuffer result = new StringBuffer();
			// 打开和URL之间的连接
			HttpsURLConnection httpURLConnection = getRemoteAccessIgnoreSSLConn(url);
			
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8");
			
			// 获得响应状态
			int resultCode = httpURLConnection.getResponseCode();
			if (HttpURLConnection.HTTP_OK == resultCode) {
				BufferedReader reader = new BufferedReader(inputStreamReader);
				String line = null;
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				reader.close();
			}
			return result.toString();
		} catch (NoSuchAlgorithmException nsae) {
			throw new WxpublicException(nsae);
		} catch (KeyManagementException kme) {
			throw new WxpublicException(kme);
		} catch (IOException ioe) {
			throw new WxpublicException(ioe);
		}
	}

	/**
	 * 发送普通的GET请求
	 * @param url
	 * @return
	 * @throws WxpublicException
	 */
	public static String sendGet(String url) throws WxpublicException {
		try {
			StringBuffer result = new StringBuffer();
			// 打开和URL之间的连接
			HttpURLConnection httpURLConnection = (HttpURLConnection) getRemoteAccessConn(url);
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8");
			// 获得响应状态
			int resultCode = httpURLConnection.getResponseCode();
			if (HttpURLConnection.HTTP_OK == resultCode) {
				BufferedReader reader = new BufferedReader(inputStreamReader);
				String line = null;
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				reader.close();
			}
			return result.toString();
		} catch (IOException ioe) {
			throw new WxpublicException(ioe);
		}
	}

	/**
	 * 配置安全的请求链接
	 * @param realUrl
	 * @return
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
    private static HttpsURLConnection getRemoteAccessSSLConn(String realUrl) throws IOException, NoSuchAlgorithmException, KeyManagementException {
		/**
		 * 创建SSLContext对象，并使用我们指定的信任管理器初始化
		 */
		TrustManager[] tm = {new MyX509TrustManager()};
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(null, tm, new SecureRandom());
		/**
		 * 从上述SSLContext对象中得到SSLSocketFactory对象
		 */
		SSLSocketFactory sf = sslContext.getSocketFactory();
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) getRemoteAccessConn(realUrl);
        httpsURLConnection.setSSLSocketFactory(sf);
        return httpsURLConnection;
    }

	/**
	 * 配置安全的请求链接 忽略HTTPS请求的SSL证书，必须在openConnection之前调用
	 * @param realUrl
	 * @return
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
    private static HttpsURLConnection getRemoteAccessIgnoreSSLConn(String realUrl) throws IOException, NoSuchAlgorithmException, KeyManagementException {
    	HostnameVerifier hv = new HostnameVerifier() {
			@Override
			public boolean verify(String urlHostName, SSLSession session) {
				return true;
			}
		};
		/**
		 * 创建SSLContext对象，并使用我们指定的信任管理器初始化
		 */
    	TrustManager[] trustAllCerts = new TrustManager[1];
    	TrustManager tm = new MyX509TrustManager();
    	trustAllCerts[0] = tm;
    	SSLContext sslContext = SSLContext.getInstance("SSL");
    	sslContext.init(null, trustAllCerts, null);
    	HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) getRemoteAccessConn(realUrl);
        return httpsURLConnection;
    }

	/**
	 * 配置请求链接
	 * @param realUrl
	 * @return
	 * @throws IOException 
	 */
    private static URLConnection getRemoteAccessConn(String realUrl) throws IOException {
        URL url = new URL(realUrl);
        URLConnection urlConnection = url.openConnection();
    	/**
		 * 以后是否使用conn.getInputStream().read()
    	 */
        urlConnection.setDoInput(true);
    	/**
		 * 以后是否使用conn.getOutputStream().write()
    	 */
        urlConnection.setDoOutput(true);
    	/**
		 * 请求是否使用缓存 
    	 */
        urlConnection.setUseCaches(false);
    	/**
		 * HTTP消息实体的传输长度
    	 */
        urlConnection.setRequestProperty("content_length", "20480");
        return urlConnection;
	}
}


class MyX509TrustManager implements TrustManager, X509TrustManager {
	
	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
    	
	}
	
	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
    	
	}
	
	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}
	
    public boolean isServerTrusted(X509Certificate[] certs) {
        return true;
    }
	
    public boolean isClientTrusted(X509Certificate[] certs) {
        return true;
    }
	
}
