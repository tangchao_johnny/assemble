package tangchao.wxpublic.wxpub;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import tangchao.wxpublic.common.util.HttpUtil;
import tangchao.wxpublic.common.util.StringUtil;
import tangchao.wxpublic.exception.WxpublicException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 唐潮(johnny)
 * 2018/12/3 15:47
 */
public class BaseAuth {
	
	/**
	 * 获取公众号的全局唯一接口调用凭据access_token
	 * 此接口调用一次的有准备期为7200秒，避免接口的重复调用，如果可以，请在服务器上做缓存。
	 * 请在快失效的情况下再调用此接口来获取新的access_token
	 *
	 * @param appId 公众号ID
	 * @param secret 密钥
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject token(String appId, String secret) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/cgi-bin/token?");
		sb.append("grant_type=client_credential");
		sb.append("&appid=").append(appId);
		sb.append("&secret=").append(secret);
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSONObject.parseObject(result);
	}
	
	/**
	 * 获取公众号用于调用微信JS接口的临时票据jsapi_ticket，
	 * 此接口调用一次的有准备期为7200秒，避免接口的重复调用，如果可以，请在服务器上做缓存。
	 * 请在快失效的情况下再调用此接口来获取新的jsapi_ticket
	 *
	 * @param accessToken 全局AccessToken
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject getticket(String accessToken) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/cgi-bin/ticket/getticket?");
		sb.append("access_token=").append(accessToken);
		sb.append("&type=jsapi");
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSONObject.parseObject(result);
	}
	
	/**
	 * JS-SDK使用权限签名
	 *
	 * @param appId       公众号ID
	 * @param jsapiTicket JS接口的临时票据
	 * @param url         要使用JSSDK的路径
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject jsSign(String appId, String jsapiTicket, String url) throws WxpublicException {
		try {
			Map<String, String> ret = new HashMap<>();
			String signature = "";
			String nonceStr = StringUtil.getUuid(true);
			String timestamp = StringUtil.createTimestamp();
			String str = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url;
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(str.getBytes("UTF-8"));
			signature = byteToHex(crypt.digest());
			ret.put("appid", appId);
			ret.put("url", url);
			ret.put("jsapi_ticket", jsapiTicket);
			ret.put("nonceStr", nonceStr);
			ret.put("timestamp", timestamp);
			ret.put("signature", signature);
			return JSON.parseObject(JSON.toJSONString(ret, SerializerFeature.WriteMapNullValue));
		} catch (NoSuchAlgorithmException nsae) {
			throw new WxpublicException(nsae);
		} catch (UnsupportedEncodingException uee) {
			throw new WxpublicException(uee);
		}
	}
	
	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}
	
}
