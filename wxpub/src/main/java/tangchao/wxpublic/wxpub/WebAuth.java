package tangchao.wxpublic.wxpub;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import tangchao.wxpublic.common.util.HttpUtil;
import tangchao.wxpublic.common.util.StringUtil;
import tangchao.wxpublic.exception.WxpublicException;
import tangchao.wxpublic.wxpub.enumerate.ScopeType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by 唐潮(johnny)
 * 2018/12/3 14:29
 */
public class WebAuth {
	
	/**
	 * 如果没有能过微信授权或者授权失效了，调用此方法再时行授权
	 * 授权后会自动跳转到回调页面，所以这个方法最好放在你的方法最后面来执行
	 *
	 * @param appId       公众号ID
	 * @param redirectUrl 回调URL
	 * @param scopeType   授权作用域
	 */
	public static String authorize(String appId, String redirectUrl, ScopeType scopeType) {
		try {
			StringBuilder sb = new StringBuilder("https://open.weixin.qq.com/connect/oauth2/authorize?");
			sb.append("appid=").append(appId);
			sb.append("&redirect_uri=").append(URLEncoder.encode(redirectUrl, "UTF-8"));
			sb.append("&response_type=code");
			sb.append("&scope=").append(scopeType.toString());
			sb.append("&state=").append(StringUtil.getUuid(true));
			sb.append("#wechat_redirect");
			return sb.toString();
		} catch (Exception e) {
			return "";
		}
	}
	
	/**
	 * 如果没有能过微信授权或者授权失效了，调用此方法再时行授权
	 * 授权后会自动跳转到回调页面，所以这个方法最好放在你的方法最后面来执行
	 *
	 * @param appId       公众号ID
	 * @param redirectUrl 回调URL
	 * @param scopeType   授权作用域
	 * @throws WxpublicException
	 */
	public static void authorize(HttpServletResponse response, String appId, String redirectUrl, ScopeType scopeType) throws WxpublicException {
		try {
			StringBuilder sb = new StringBuilder("https://open.weixin.qq.com/connect/oauth2/authorize?");
			sb.append("appid=").append(appId);
			sb.append("&redirect_uri=").append(URLEncoder.encode(redirectUrl, "UTF-8"));
			sb.append("&response_type=code");
			sb.append("&scope=").append(scopeType.toString());
			sb.append("&state=").append(StringUtil.getUuid(true));
			sb.append("#wechat_redirect");
			response.sendRedirect(sb.toString());
		} catch (Exception e) {
			throw new WxpublicException(e);
		}
	}
	
	/**
	 * 通过code换取网页授权access_token
	 *
	 * @param appId  公众号ID
	 * @param secret 公众号令牌
	 * @param code   通过authorize接口加调拿到的code
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject access_token(String appId, String secret, String code) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/sns/oauth2/access_token?");
		sb.append("appid=").append(appId);
		sb.append("&secret=").append(secret);
		sb.append("&code=").append(code);
		sb.append("&grant_type=authorization_code");
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSON.parseObject(result);
	}
	
	/**
	 * 刷新AccessToken 授权AccessToken快失效时，通过此方法来刷新AccessToken，使有效期延长
	 *
	 * @param appId        公众号ID
	 * @param refreshToken 用来刷新AccessToken的令牌
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject access_token(String appId, String refreshToken) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/sns/oauth2/refresh_token?");
		sb.append("appid=").append(appId);
		sb.append("&refresh_token=").append(refreshToken);
		sb.append("&grant_type=refresh_token");
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSON.parseObject(result);
	}
	
	/**
	 * 拉取用户信息(需scope为 snsapi_userinfo)
	 *
	 * @param accessToken 授权令牌
	 * @param openId      用户的openId
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject userinfo(String accessToken, String openId) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/sns/userinfo?");
		sb.append("access_token=").append(accessToken);
		sb.append("&openid=").append(openId);
		sb.append("&lang=zh_CN");
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSON.parseObject(result);
	}
	
	/**
	 * 检验授权凭证（access_token）是否有效
	 *
	 * @param accessToken 授权令牌
	 * @param openId      用户的openId
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject auth(String accessToken, String openId) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/sns/auth?");
		sb.append("access_token=").append(accessToken);
		sb.append("&openid=").append(openId);
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSON.parseObject(result);
	}
	
}
