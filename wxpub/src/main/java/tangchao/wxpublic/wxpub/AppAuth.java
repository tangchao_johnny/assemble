package tangchao.wxpublic.wxpub;

import com.alibaba.fastjson.JSONObject;
import tangchao.wxpublic.common.util.HttpUtil;
import tangchao.wxpublic.exception.WxpublicException;

/**
 * Created by 唐潮(johnny)
 * 2019/1/3 13:18
 */
public class AppAuth {
	
	/**
	 * 登录凭证校验
	 * @param appId     应用ID
	 * @param secret    应用密钥
	 * @param jsCode    JSCODE
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject jscode2session(String appId, String secret, String jsCode) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/sns/jscode2session?");
		sb.append("appid=").append(appId);
		sb.append("&secret=").append(secret);
		sb.append("&js_code=").append(jsCode);
		sb.append("&grant_type=authorization_code");
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSONObject.parseObject(result);
	}
	
	/**
	 * 获取小程序全局唯一后台接口调用凭据
	 * @param appId     应用ID
	 * @param secret    应用密钥
	 * @return
	 * @throws WxpublicException
	 */
	public static JSONObject token(String appId, String secret) throws WxpublicException {
		StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/cgi-bin/token?");
		sb.append("grant_type=client_credential");
		sb.append("&appid=").append(appId);
		sb.append("&secret=").append(secret);
		String result = HttpUtil.sendGetSSL(sb.toString());
		return JSONObject.parseObject(result);
	}
	
}
