package tangchao.wxpublic.wxpub.enumerate;

/**
 * Created by 唐潮(johnny)
 * 2018/12/3 14:34
 */
public enum ScopeType {
	snsapi_base,
	snsapi_userinfo
}
